Source: hyperkitty
Section: python
Priority: optional
Maintainer: Debian Mailman Team <pkg-mailman-hackers@lists.alioth.debian.org>
Uploaders: Pierre-Elliott Bécue <peb@debian.org>,
           Jonas Meurer <jonas@freesources.org>
Build-Depends: debhelper-compat (= 13),
               dh-python,
               pybuild-plugin-pyproject,
               python3-all,
               python3-bs4,
               python3-dateutil,
               python3-django,
               python3-django-compressor,
               python3-django-extensions,
               python3-django-gravatar2,
               python3-django-haystack,
               python3-django-mailman3,
               python3-django-q,
               python3-djangorestframework,
               python3-elasticsearch,
               python3-flufl.lock,
               python3-isort,
               python3-lxml,
               python3-mailmanclient,
               python3-mistune,
               python3-networkx,
               python3-pdm-backend,
               python3-robot-detection,
               python3-tz,
               python3-whoosh,
               python3-sphinx,
               python3-sphinx-rtd-theme,
               sassc
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: https://gitlab.com/mailman/hyperkitty
Vcs-Browser: https://salsa.debian.org/mailman-team/hyperkitty
Vcs-Git: https://salsa.debian.org/mailman-team/hyperkitty.git/

Package: python3-django-hyperkitty
Architecture: all
Depends: fonts-font-awesome,
         fonts-glyphicons-halflings,
         libjs-jquery,
         libjs-jquery-ui,
         node-bootstrap,
         python3-dateutil,
         python3-django,
         python3-django-compressor,
         python3-django-extensions,
         python3-django-gravatar2,
         python3-django-haystack,
         python3-django-mailman3,
         python3-django-q,
         python3-djangorestframework,
         python3-elasticsearch,
         python3-flufl.lock,
         python3-mailmanclient,
         python3-mistune,
         python3-networkx,
         python3-robot-detection,
         python3-tz,
         ${misc:Depends},
         ${python3:Depends},
         ${sphinxdoc:Depends}
Recommends: mailman3-web
Breaks: mailman3-web (<< 0+20180916-10~)
Conflicts: python-django-hyperkitty
Replaces: python-django-hyperkitty
Description: Web user interface to access GNU Mailman3 archives
 The hyperkitty Django app provides a web user interface
 to access GNU Mailman3 archives, and manage it. This
 interface uses django, and requires some configuration.
